# Benjamin Patch
Public website for benjaminpatch.com, built with [Middleman](https://middlemanapp.com/).

## Ruby Dev Environment: Linux
- Install rbenv with ruby-build
- Install desired version of Ruby
- Set global and local Ruby versions

```
$ gem install bundler
```

Install missing gem packages:

```
$ bundle install
```

Update gem packages if desired:

```
$ bundle update
```

Launch Middleman server:

```
$ bundle exec middleman server
```

## Ruby Dev Environment: Windows 10
### Linux Subsystem
* https://docs.microsoft.com/en-us/windows/wsl/install-win10
* https://gorails.com/setup/windows/10

### Bundler Reference
* http://bundler.io

### Node.js for Middleman Server
* https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions

### Local Filepath
* /mnt/c/websites/benjaminpatch.com

---

## Release Notes

### 1.5.0
* 08 February 2020.
* Update to Ruby 2.6.5.

### 1.4.0
* 04 June 2019.
* Update content with more focus on Avid certification and The Post Workshop.

### 1.3.0
* 31 March 2018.
* Add 'Astray' trailer and 'Cold Light' to filmography.

### 1.2.0
* 05 August 2017.
* Add 'Choice Words' and 'Doppler Salad' to filmography.
* Update headshot.

### 1.1.1
* 27 March 2016.
* Minor grammar adjustment.

### 1.1.0
* 22 January 2016.
* Rewrite copy to express cinema major and history minor.
* Refine typography and layout.

### 1.0.1
* 26 June 2015.
* Minor content edits.
* Activate clean URLs.

### 1.0.0
* 18 June 2015.
* Initial public release.
* HTTPS everywhere.
* Google Analytics installed and tested.
* Nginx config includes related domain redirects.
* Sitemap submitted to Google, Bing, and Yandex.
